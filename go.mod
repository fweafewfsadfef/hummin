module gitlab.com/trantor/hummin

go 1.14

require (
	github.com/gobs/args v0.0.0-20180315064131-86002b4df18c // indirect
	github.com/gobs/cmd v0.0.0-20190925005709-491cb99ce818
	github.com/gobs/pretty v0.0.0-20180724170744-09732c25a95b // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/peterh/liner v1.2.0 // indirect
	github.com/ttacon/chalk v0.0.0-20160626202418-22c06c80ed31
	gopkg.in/gcfg.v1 v1.2.3
	gopkg.in/warnings.v0 v0.1.2 // indirect
	h12.io/socks v1.0.0
)
