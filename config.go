package main

const (
	BASE_URL         = "http://kx5thpx2olielkihfyo4jgjqfb7zx7wxr3sd4xzt26ochei4m6f7tayd.onion/"
	PROXY            = "127.0.0.1:9050"
	DOWNLOAD_WORKERS = 3
	UPLOAD_WORKERS   = 3
	CONFIG_FILE      = "~/.humminrc"
)
